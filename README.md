# Space Rescue - A Escape Coding Adventure

A pyray game for escape coding adventure. This repository is the player repository that will contains the player code
and resources.

## Description

This game allows to build an adventure where the player must unlock challenges by coding a solution to a
problem. The system will check the validity of the result (possible result and test coverage > 80%) and let the player
naviguate in the game.

### Mission

You are the AI of the NCC-DaFa spaceship, the state-of-the-art ship of the Earth fleet. After an accident, the crew is
disabled and must be bring back to a planet to be rescued. Following your directives, you have to plot a safe course to
the planet indicated by the Earth Command. Earth Command sent a message in your logs when they became aware of the
situation.

## Getting Started

### Dependencies

* Python 3.11 or above
* Poetry 1.7.1 or above

### Python Installation

For more details, see the [Installation Guide](https://www.python.org/)

### Poetry Installation

Install using pip:

```bash
pip install poetry
```

For more details, see the [Installation Guide](https://python-poetry.org/docs/)

### Build and install locally

Fork and clone the GitLab project:

```bash
git clone git@gitlab.com:romualdrousseau/hackathon.git
```

Some resources are on LFS. The total size is around 2BG. Please use this command to download them:

```bash
git lfs install
git lfs pull
```

Create a new branch with your name:

```bash
git checkout -b ＜new-branch-your-name＞
```

Install all dependencies with the following command:

```bash
poetry install --no-root
```

### Run the game framework

Run the game with a game_seed XX with the following command:

```bash
poetry run python -m spacerescue -g XX
```

### Documentation


## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors

* Romuald Rousseau, romuald.rousseau@servier.com

## Version History

* Initial Release